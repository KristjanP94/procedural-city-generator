# Procedural City Generator #

Procedural City Generator is made using Three.js Javascript library. It comes with built-in terrain generator.  
Parameters can be adjusted to generate different cities.

## Demo: [City Generator](http://morsakabi.com/baka/) ##
Use WASD buttons and mouse to control camera.