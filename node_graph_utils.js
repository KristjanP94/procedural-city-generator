//Copyright (C) 2016  Kristjan Perli

//Constants
var cycleLengthLimit = 6;
var lotShortestEdge = 25;

//Other
var filteredSegments = {};
var removableSegments = [];
var startNodes = [];
var filtered = false;
var plots_ready = false;
var unfiltered_cycles = [];
var cycles = [];
var checkCycleNum = 0;

function resetNodeGraph() {
	filteredSegments = {};
	removableSegments = [];
	startNodes = [];
	filtered = false;
	plots_ready = false;
	unfiltered_cycles = [];
	cycles = [];
}


function extractCycles() {

	filterSegments();
	
	for(var i = 0; i < startNodes.length; i++) {	
		findCycles([parseInt(startNodes[i])]);
	}

	unfiltered_cycles = unfiltered_cycles.unique2();
	cycles = filterCycles(unfiltered_cycles);
	subdivideLots();
	plots_ready = true;
}

//Removes excessive segments - the ones that can't be part of any cycle
function filterSegments() {
	
	//Remove segments which are dead ends
	for(var s = 0; s < nodes.length; s++) {
		if(!(s in segments)) {
			continue;
		}
		if(segments[s].length > 1) {
			filteredSegments[s] = segments[s];
		} else {
			removableSegments.push([s, segments[s][0]]);
		}
	}
	
	var filterRound = 1;
	
	//Removing previous dead ends probably created new dead ends which were connected to previous ones
	while(filtered == false) {
		filterRound ++;
		filtered = true;
		if(filterRound > 100) {
			break;
		}
		
		//Remove dead ends
		for(var r = 0; r < removableSegments.length; r++) {		
			if(filteredSegments[removableSegments[r][1]] == undefined) {
				if(filteredSegments[removableSegments[r][0]] != undefined) {
					var index = filteredSegments[removableSegments[r][0]].indexOf(removableSegments[r][1]);
				}			
				continue;
			}				
		}
		removableSegments = [];
		
		//Find new dead ends
		for(s in filteredSegments) {
			if(filteredSegments[s].length == 1) {
				removableSegments.push([parseInt(s), filteredSegments[s][0]]);
				filtered = false;
			 } else if(filteredSegments[s].length == 0) {
				 delete filteredSegments[s];
			 }
		}	
	}
	
	var count = 0;
	//Each cycle has at least one node which is connected to 3 other nodes
	for(s in filteredSegments) {
		count++;
		if(filteredSegments[s].length > 2) {
			startNodes.push(s);
		}
	}	
}

//Find cycles by recursively going through graph
function findCycles(current) {
	
	if(current.length > cycleLengthLimit) {
		return;
	}

	var nextNodes = filteredSegments[current[current.length - 1]];
	if(nextNodes == undefined) {
		return;
	}
	for(var j = 0; j < nextNodes.length; j++) {
		if(current.indexOf(nextNodes[j]) < 0) {
			current.push(nextNodes[j]);
			findCycles(current);
			current.pop();
		}
		//Reached cycle startPoint
		else if(current.indexOf(nextNodes[j]) == 0 && current.length > 2) {
			
			if(checkCycle(current)) {
				unfiltered_cycles.push(current.slice().sort());
			}			
		}	
	}
}


//Check if any non sequential nodes are connected
function checkCycle(cycle) {
	
	if(cycle.length < 4) {
		return true;
	}

	for(var i = 0; i < cycle.length; i++) {
		for(var j = 0; j < cycle.length; j++) {
			if(i == j) {
				continue;
			}		
			if((i == 0 && j == cycle.length - 1) || (j == 0 && i == cycle.length - 1)) {
				continue;
			}
			
			//Check if any non sequential nodes are connected
			if(Math.abs(i - j) > 1) {
				if(filteredSegments[cycle[i]].indexOf(cycle[j]) >= 0) {
					return false;
				}
			}		
		}
	}
	
	return true;
}

//Removes cycles with 3+ shared nodes, leaves smallest cycles
function filterCycles(unfiltered_cycles) {
		
	var removeCycles = [];
	var filterRound = 0;
	filtered = false;
	while(filtered == false) {
		filterRound ++;
		filtered = true;
		if(filterRound > 100) {
			break;
		}

		for(var r = 0; r < unfiltered_cycles.length - 1; r++) {
			for(var k = r + 1; k < unfiltered_cycles.length; k++) {

				if(unfiltered_cycles[r].compare2(unfiltered_cycles[k])) {
					if(removeCycles.indexOf(unfiltered_cycles[k]) < 0) {
						removeCycles.push(unfiltered_cycles[k]);
					}				
					filtered = false;
				} else if(unfiltered_cycles[k].compare2(unfiltered_cycles[r])) {
					if(removeCycles.indexOf(unfiltered_cycles[r]) < 0) {
						removeCycles.push(unfiltered_cycles[r]);
					}	
					filtered = false;
				}
			}
		}

		if(filtered == false) {
			for(var i = 0; i < removeCycles.length; i++) {
				var index = unfiltered_cycles.indexOf(removeCycles[i]);
				unfiltered_cycles.splice(index, 1);
			}
			removeCycles = [];
		}
	}
	
	var filteredCycles = [];
	var correctCycle = true;
	
	//Some cycles might contain nodes inside
	for(var i = 0; i < unfiltered_cycles.length; i++) {
		if(unfiltered_cycles[i].length == 3) {
			filteredCycles.push(unfiltered_cycles[i]);
		} else {
			var outsideNodes = {};
			correctCycle = true;
			for(var j = 0; j < unfiltered_cycles[i].length; j++) {
				for(var k = 0; k < filteredSegments[unfiltered_cycles[i][j]].length; k++) {
					//Add nodes that aren't in cycle, but can be reached from nodes of cycle
					if(unfiltered_cycles[i].indexOf(filteredSegments[unfiltered_cycles[i][j]][k]) < 0) {
						if(filteredSegments[unfiltered_cycles[i][j]][k] in outsideNodes) {
							outsideNodes[filteredSegments[unfiltered_cycles[i][j]][k]] ++;
						} else {
							outsideNodes[filteredSegments[unfiltered_cycles[i][j]][k]] = 1;
						}
					} 
				}		
			}
			for(node in outsideNodes) {
				if(outsideNodes[node] > 2) {
					correctCycle = false;
				}
			}
			if(correctCycle) {
				filteredCycles.push(unfiltered_cycles[i]);
			}
		}
	}

	//Remove cycles where all nodes are invisible - not connected to road)
	for(var i = filteredCycles.length - 1; i >= 0; i--) {
		correctCycle = false;
		for(var j = 0; j < filteredCycles[i].length; j++) {
			if(invisibleNodes.indexOf(nodes[filteredCycles[i][j]]) == -1) {
				correctCycle = true;
				break;
			} 
		}
		if(correctCycle == false) {
			filteredCycles.splice(i, 1);
		}
	}
	
	return filteredCycles;
}


//Given a bounding cycle, insets and subdivides a block into lots 
function subdivideLots() {
	
	for(var b = 0; b < cycles.length; b++) {
		var points = [];
		
		for(var c = 0; c < cycles[b].length; c++) {
			points.push(new THREE.Vector2 (nodes[cycles[b][c]].x, nodes[cycles[b][c]].z));
		}
		
		var center = sortVertices(points, true);
		if(calcPolygonArea(points) < 3000) {
				continue;
		}
		points = insetPolygon(points, primaryRoadWidth / 2);
		

		var lots = [];
		var roadPoints = [points.slice(), []];
		subdivide(points, center, lots, roadPoints);

		//Remove lots that dont have road access
		for(var i = 0; i < lots.length; i++) {
			for(var j = lots[i].length - 1; j >= 0; j--) {
				if(roadPoints[0].indexOf(lots[i][j]) >= 0 || roadPoints[1].indexOf(lots[i][j]) >= 0) {
					break;
				}
				if(j == 0) {
					lots.splice(i, 1);
				}
			}
		}
		blocks.push([center, lots]);
	}
	main.scene.children[2].geometry.colorsNeedUpdate = true;
}



//Subdivides 
function subdivide(points, center, polygons, roadPoints) {
	
	var shortestEdge = lotShortestEdge + Math.random() * 15;
	var longestEdge = [points.length - 1, 0];
	var edgeLength = lineDistance(points[points.length - 1], points[0]);
	//Find longest edge
	for(var e = 0; e < points.length - 1; e++) {	
		
		if(lineDistance(points[e], points[e + 1]) < lotShortestEdge) {						
			if(calcPolygonArea(points) > 2000) {
				continue;
			}					
			polygons.push(points);
			return;
		}		
		
		if(lineDistance(points[e], points[e + 1]) > edgeLength) {
			edgeLength = lineDistance(points[e], points[e + 1]);
			longestEdge = [e, e + 1];
		}
	}

	var point1Weight = 0.5 + Math.random() * 0.3 - 0.15;
	var longestEdgeMidpoint = new THREE.Vector2((points[longestEdge[0]].x * point1Weight + points[longestEdge[1]].x * (1 - point1Weight)) , (points[longestEdge[0]].y * point1Weight + points[longestEdge[1]].y * (1 - point1Weight)));
	//Midpoint between 2 roadPoints is roadPoint
	if(roadPoints[0].indexOf(points[longestEdge[0]]) >= 0 && roadPoints[0].indexOf(points[longestEdge[1]]) >= 0) {	
		roadPoints[1].push(longestEdgeMidpoint);
	}
	var angle = toDegrees(Math.atan2(points[longestEdge[1]].y - points[longestEdge[0]].y , points[longestEdge[1]].x - points[longestEdge[0]].x));
	var perpendicularEdgePoint = new THREE.Vector2(longestEdgeMidpoint.x + Math.cos(toRadians(angle + 90)) * 200, longestEdgeMidpoint.y + Math.sin(toRadians(angle + 90)) * 200);
	
	var intersection;
	var intersectionPoints = [];
	var intersectionIndex = 0;
	for(var e = 0; e < points.length; e++) {	
		if(e == longestEdge[0]) {
			continue;
		}
		if(e == points.length - 1) {
			intersection = lineIntersectionPoint(longestEdgeMidpoint.x, longestEdgeMidpoint.y, perpendicularEdgePoint.x, perpendicularEdgePoint.y, points[e].x, points[e].y, points[0].x, points[0].y);
			if(intersection != undefined) {
				intersectionIndex = e;
				intersectionPoints = [e, 0];
				break;
			}
		} else {
			intersection = lineIntersectionPoint(longestEdgeMidpoint.x, longestEdgeMidpoint.y, perpendicularEdgePoint.x, perpendicularEdgePoint.y, points[e].x, points[e].y, points[e + 1].x, points[e + 1].y);		
			if(intersection != undefined) {
				intersectionIndex = e;
				intersectionPoints = [e, e + 1];
				break;
			}
		}
	}

	//Midpoint between 2 roadPoints is roadPoint
	if(roadPoints[0].indexOf(points[intersectionPoints[0]]) >= 0 && roadPoints[0].indexOf(points[intersectionPoints[1]]) >= 0) {
		roadPoints[1].push(intersection);
	}
	
	points.splice(longestEdge[0] + 1, 0, longestEdgeMidpoint);
	longestEdge[0] ++;
	
	//Create divided polygons
	var polygon1, polygon2;
	if(longestEdge[0] <= intersectionIndex) {
		intersectionIndex ++;
		points.splice(intersectionIndex + 1, 0, intersection);		
		polygon1 = points.slice(longestEdge[0], intersectionIndex + 2);		
		points.splice(longestEdge[0] + 1, intersectionIndex - longestEdge[0]);
		polygon2 = points.slice();
	} else {
		longestEdge[0] ++;
		intersectionIndex ++;
		points.splice(intersectionIndex, 0, intersection);
		polygon1 = points.slice(intersectionIndex, longestEdge[0] + 1);
		points.splice(intersectionIndex + 1, longestEdge[0] - intersectionIndex - 1);
		polygon2 = points.slice();
	}
	
	//Subdivide the new polygons
	subdivide(polygon1, center, polygons, roadPoints);
	subdivide(polygon2, center, polygons, roadPoints);
}