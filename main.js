//Copyright (C) 2016  Kristjan Perli

function onLoad() {
    var main = new Main();
    main.initialize();
    this.main = main;
	
    initializeViewport(1280, 720);
	initializeUI();
	loadTextures();
	loadTerrain();
	generateRoads();
    draw();
}


function initializeViewport(width, height) {
 
	renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(width, height);
    renderer.setClearColor(0xeeeeee, 1);
    canvasContainer.appendChild(renderer.domElement);

    var lookAtVector = new THREE.Vector3(0, 0, 200);
    var camera = new THREE.PerspectiveCamera( 70, width / height, 1, 5000 );
    
    camera.position.x = 0;
	camera.position.y = 1500;
    camera.position.z = 500;
    camera.lookAt(lookAtVector);
    camera.updateProjectionMatrix();
    main.camera = camera;
    main.lookAtVector = lookAtVector;

    var controls = new THREE.OrbitControls(camera, this.renderer.domElement);
    main.controls = controls;
}

function initializeUI() {
	
    var Text = function() {
        this.generationSpeed = 35;
		this.waterPercentMin = 0;
		this.waterPercentMax = 40;
		this.newTerrain = false;
		this.randomSeed = false;
		this.seed = 0;
        this.GENERATE = function() {
			if(this.randomSeed) {
				Math.seedrandom();
			} else {
				Math.seedrandom(this.seed);
			}

			for(var k = main.scene.children.length - 1; k > 3; k--) {
				main.scene.remove(main.scene.children[k]);
			}
			
			waterPercentMin = this.waterPercentMin;
			waterPercentMax = this.waterPercentMax;
			if(waterPercentMax < waterPercentMin + 5) {
				waterPercentMax = waterPercentMin + 5;
			}
			if(this.newTerrain) {
				main.scene.remove(main.scene.children[3]);
				main.scene.remove(main.scene.children[2]);
				resetTerrain();
				loadTerrain();
			} else {
				resetFlatAreas();
			}
			
			main.buildingsReady = false;
			main.roadsReady = false;
			main.generationSpeed = this.generationSpeed;
			resetRoads();
			resetNodeGraph();
			resetBuildingGenerator();
			primaryGridStyleCutoffDensity = this.primaryGridStyleCutoffDensity;
			primaryLeftChance = this.primaryLeftChance;
			primaryRightChance = this.primaryRightChance;
			primaryBothChance = this.primaryBothChance;
			primarySplitChance = this.primarySplitChance;
			maxPrimaryNodes = this.maxPrimaryNodes;
			maxSecondaryNodes = this.maxSecondaryNodes;
			downtownBuildingMaxHeight = this.downtownBuildingMaxHeight;
			buildingMaxHeight = this.buildingMaxHeight;
			main.createBuildings = this.createBuildings;
			generateRoads();
        };
		this.primaryGridStyleCutoffDensity = 0.8;
		this.primaryLeftChance = 0.04;
		this.primaryRightChance = 0.04;
		this.primaryBothChance = 0.04;
		this.primarySplitChance = 0.02;
		this.maxPrimaryNodes = 800;
		this.maxSecondaryNodes = 1000;
		this.createBuildings = true;
		this.downtownBuildingMaxHeight = 190;
		this.buildingMaxHeight = 70;
    };
    
    var gui = new dat.GUI();  
    var f1 = gui.addFolder('General');
    var text = new Text();
    f1.add(text, 'generationSpeed', 1, 100).step(1).title('Road segments / buildings per frame');
	f1.add(text, 'waterPercentMin', 0, 60).step(1);
	f1.add(text, 'waterPercentMax', 0, 80).step(1);
	f1.add(text, 'newTerrain').title('Creates new terrain');
	f1.add(text, 'randomSeed').title('Uses random seed');
	f1.add(text, 'seed');
    f1.add(text, 'GENERATE');
	var f2 = gui.addFolder('Roadmap');
	f2.add(text, 'primaryGridStyleCutoffDensity', 0.1, 0.99).step(0.01).title('Primary roads grow in grid pattern when density is higher than cutoff');
	f2.add(text, 'primaryLeftChance', 0.00, 0.30).step(0.01).title('Probability of primary road branching left');
	f2.add(text, 'primaryRightChance', 0.00, 0.30).step(0.01).title('Probability of primary road branching right');
	f2.add(text, 'primaryBothChance', 0.00, 0.30).step(0.01).title('Probability of primary road branching both left and right');
	f2.add(text, 'primarySplitChance', 0.00, 0.30).step(0.01).title('Probability of primary road splitting instead of growing straight');
	f2.add(text, 'maxPrimaryNodes', 50, 1000).step(1).title('Max number of primary road nodes');
	f2.add(text, 'maxSecondaryNodes', 50, 2000).step(1).title('Max number of secondary road nodes');
	var f3 = gui.addFolder('Buildings');
	f3.add(text, 'createBuildings');
	f3.add(text, 'downtownBuildingMaxHeight', 140, 250).step(5).title('Max height of downtown buildings');
	f3.add(text, 'buildingMaxHeight', 60, 120).step(5).title('Max height of non-downtown buildings');
    //f1.open();  
}

function draw() {
	
	//Real-time generation of road segments and buildings
	if(main.buildingsReady == false && main.paused == false) {
		var count = main.generationSpeed;
		if(main.roadsReady == false) {
			while(count > 0) {
				if(loadNextRoad() == false) {
					main.roadsReady = true;
					//main.paused = true;
					break;
				}
				count--;
			}
		} else if(main.createBuildings && main.paused == false) {	
			while(count > 0) {
				if(generateNextBuilding() == false) {
					main.buildingsReady = true;
					break;
				}
				count--;
			}
		}
	}
    requestAnimationFrame(draw);
	
	main.controls.update();
	main.keyboardUpdate(main.camera);

    renderer.render(main.scene, main.camera);
}


var Main = function () {

    this.scene;
    this.keyboard;
	this.roadsReady = false;
	this.buildingsReady = false;
	this.createBuildings = true;
	this.generationSpeed = 35;
	this.paused = false;

    this.initialize = function () {
		
        var keyboard = new THREEx.KeyboardState();
        this.keyboard = keyboard;

        var scene = new THREE.Scene();
		scene.fog = new THREE.FogExp2( 0xefd1b5, 0.0003 );
        this.scene = scene;

        var ambientLight = new THREE.AmbientLight(0x444444);
        scene.add(ambientLight);

        var directionalLight = new THREE.DirectionalLight(0xffffff, 1.0);
        directionalLight.position.set(0.9, 0.9, 0.0);
        scene.add(directionalLight);

    }
	
	this.keyboardUpdate = function (camera) {

        if (this.keyboard.pressed("w")) {
            this.lookAtVector.x += camera.getWorldDirection().x * 5.0;
            this.lookAtVector.z += camera.getWorldDirection().z * 5.0;
            camera.position.x += camera.getWorldDirection().x * 5.0;
             camera.position.z += camera.getWorldDirection().z * 5.0;
        } else if (this.keyboard.pressed("s")) {
            this.lookAtVector.x -= camera.getWorldDirection().x * 25.0;
            this.lookAtVector.z -= camera.getWorldDirection().z * 25.0;
            camera.position.x -= camera.getWorldDirection().x * 25.0;
            camera.position.z -= camera.getWorldDirection().z * 25.0;
        } else if (this.keyboard.pressed("a")) {
            this.lookAtVector.x += camera.getWorldDirection().z * 25.0;
            this.lookAtVector.z -= camera.getWorldDirection().x * 25.0;
            camera.position.x += camera.getWorldDirection().z * 25.0;
            camera.position.z -= camera.getWorldDirection().x * 25.0;
        } else if (this.keyboard.pressed("d")) {
            this.lookAtVector.x -= camera.getWorldDirection().z * 25.0;
            this.lookAtVector.z += camera.getWorldDirection().x * 25.0;
            camera.position.x -= camera.getWorldDirection().z * 25.0;
             camera.position.z += camera.getWorldDirection().x * 25.0;
        } else if (this.keyboard.pressed("d")) {
            this.lookAtVector.x -= camera.getWorldDirection().z * 25.0;
            this.lookAtVector.z += camera.getWorldDirection().x * 25.0;
            camera.position.x -= camera.getWorldDirection().z * 25.0;
             camera.position.z += camera.getWorldDirection().x * 25.0;
        } else if (this.keyboard.pressed("space")) {
            this.paused = false;
        } else if (this.keyboard.pressed("q")) {
            this.controls.autoRotate = true;
        }
        
        this.controls.center = this.lookAtVector;
        camera.lookAt(this.lookAtVector);
    }

    return this;
}




