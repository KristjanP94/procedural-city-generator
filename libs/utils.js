
function toDegrees (angle) {
  return angle * (180 / Math.PI);
}

function toRadians (angle) {
  return angle * (Math.PI / 180);
}

//Works for both coordinates, assuming width = height
function getTerrainCoord(a) {
	return Math.round((a + 1280) / 5);
}

//For Vector3
function getAngle(p1, p2) {
	return toDegrees(Math.atan2(p2.z - p1.z , p2.x - p1.x)) + 90;
}

//For Vector2
function getAngleVec2(p1, p2) {
	return toDegrees(Math.atan2(p2.y - p1.y , p2.x - p1.x));
}

function getAnglePoints(p1, p2) {
	return toDegrees(Math.atan2(p2[1] - p1[1] , p2[0] - p1[0]));
}

function find_angle(A,B,C) {
    var AB = Math.sqrt(Math.pow(B[0]-A[0],2)+ Math.pow(B[1]-A[1],2));    
    var BC = Math.sqrt(Math.pow(B[0]-C[0],2)+ Math.pow(B[1]-C[1],2)); 
    var AC = Math.sqrt(Math.pow(C[0]-A[0],2)+ Math.pow(C[1]-A[1],2));
    return Math.acos((BC*BC+AB*AB-AC*AC)/(2*BC*AB));
}

function distanceVec2(p1, p2) {
	return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
}

//Distance for nodes (x and z coords)
function distance(p1, p2) {
	return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.z-p2.z)*(p1.z-p2.z));
}

//Distance for points (x and y coords are [0] and [1])
function distancePoints(p1, p2) {
	return Math.sqrt((p1[0]-p2[0])*(p1[0]-p2[0]) + (p1[1]-p2[1])*(p1[1]-p2[1]));
}

function lineDistance(point1, point2) {
    var xs = 0;
    var ys = 0;

    xs = point2.x - point1.x;
    xs = xs * xs;

    ys = point2.y - point1.y;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
}

Array.prototype.unique2 = function()
{
	var n = {},r=[];
	for(var i = 0; i < this.length; i++) 
	{
		if (!n[this[i]]) 
		{
			n[this[i]] = true; 
			r.push(this[i]); 
		}
	}
	return r;
}

//Returns true if arrays share 3 
Array.prototype.compare2 = function(testArr) {
    if (this.length <= testArr.length) {
		var countSame = 0;
		for (var i = 0; i < this.length; i++) {
			for (var j = 0; j < testArr.length; j++) {
					if(this[i] == testArr[j]) {
						countSame ++;
					}
			}
		}
		return countSame >= 3;
	} else {
		return false;
	}
}

//Given line1 startpoint(x1, y1), line1 endpoint(x2, y2); line 2 .. , returns true if lines collide
function lineIntersect(x1,y1,x2,y2, x3,y3,x4,y4) {
    var x=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    var y=((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    if (isNaN(x)||isNaN(y)) {
		
        return false;
    } else {
        if (x1>=x2) {
            if (!(x2<=x&&x<=x1)) {return false;}
        } else {
            if (!(x1<=x&&x<=x2)) {return false;}
        }
        if (y1>=y2) {
            if (!(y2<=y&&y<=y1)) {return false;}
        } else {
            if (!(y1<=y&&y<=y2)) {return false;}
        }
        if (x3>=x4) {
            if (!(x4<=x&&x<=x3)) {return false;}
        } else {
            if (!(x3<=x&&x<=x4)) {return false;}
        }
        if (y3>=y4) {
            if (!(y4<=y&&y<=y3)) {return false;}
        } else {
            if (!(y3<=y&&y<=y4)) {return false;}
        }
    }
    return true;
}

function lineIntersectionPoint(x1,y1,x2,y2, x3,y3,x4,y4) {
	
	if(lineIntersect == false) {
		return;
	}
	var d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
	if (d == 0) return;
	
	var xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/ d;
	var yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/ d;
    var x = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    var y = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
	
	if(x < x3 && x < x4 || x > x3 && x > x4 || y < y3 && y < y4 || y > y3 && y > y4) {
		return;
	}
	
    return new THREE.Vector2(x, y);
}

