var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var width = 512;
var height = 512;
this.max = width;

context.fillStyle = "#000000";
context.fillRect(0,0,width, height);



for(var i = 0; i < centers.length; i++) {
	console.log(centers[i]);
}


var img = context.getImageData(0, 0, width, height);

for(var x = 0; x < width; x++) {
	for(var y = 0; y < height; y++) {
		for(var z = 0; z < centers.length; z++) {
			//var closestCenter = getClosestCenter(x, y);	
			var distanceToCenter = getDistanceToCenter(x, y, centers[z]);
		
			var distanceNormalized = (1 - distanceToCenter/width);
			distanceNormalized *= distanceNormalized;
			
			var white = 0;
			
			
			if(distanceNormalized > 0.99) {
				white = 1;
			} else {
				white = distanceNormalized / 2 + getPixel(x, y) / 256.0;
				if(white > 0.95) {
					white = 0.95;
				}
			}	
		
			setPixel(x, y, white * 256, white * 256, white * 256);
			console.log(x + " - " + y + " " + getPixel(x, y));
		}		
	}
}

function getClosestCenter(x, y) {
	var closest = 0;
	var closestDistance = width + 1;
	for(var i = 0; i < centers.length; i++) {
		var distance = getDistanceToCenter(x, y, centers[i]);
		if(distance < closestDistance) {
			closest = i;
			closestDistance = distance;
		}
	}
	return centers[closest];
}

function getDistanceToCenter(x, y, center) {
	var deltaX = center[0] - x;
	var deltaY = center[1] - y;

	return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
}

context.putImageData(img, 0, 0);

function getPixel(x, y) {
	return img.data[(y * img.width + x) * 4];
}

function setPixel(x, y, r, g, b) {
	img.data[(y * img.width + x) * 4] = r;
	img.data[(y * img.width + x) * 4 + 1] = g;
	img.data[(y * img.width + x) * 4 + 2] = b;
}