//Copyright (C) 2016  Kristjan Perli

var debugNodes = false;
var maxPrimaryNodes = 800;
var maxSecondaryNodes = 900;
var invisibleSecondaryRoads = false; //Create invisible roads to generate buildings with no visible road around it
var segmentLength = 90.0;
var longestBridge = 400.0;
var primaryRoadWidth = 20;
var secondaryRoadWidth = 12;

//Secondary roads
var leftChance = 0.10;
var rightChance = 0.10;
var bothChance = 0.3;
var forwardChance = 0.7;

//Primary roads
var primaryLeftChance = 0.4;
var primaryRightChance = 0.04;
var primaryBothChance = 0.04;
var primarySplitChance = 0.02;
var primaryStraightChance = 0.5; //For organic road, makes road less curvy
var primaryGridStyleCutoffDensity = 0.8;

//Node graph
var secondaryNodesCreated = 0;
var secondaryCreated = false;
var primaryCreated = false;
var invisibleNodes = [];
var bridgeNodes = [];
var tempNodes = [];
var nodes = [];
var primaryNodes = [];
var nodeAngles = [];
var nodeAngleDeltas = [];
var primaryIntersections = [];
var segments = {};
var segmentQueue = [];


var RoadType = {
  PRIMARY_ORGANIC: 1,
  PRIMARY_GRID: 2,
  SECONDARY_GRID: 3,
  BRIDGE: 4,
};

function resetRoads() {
	secondaryNodesCreated = 0;
	secondaryCreated = false;
	primaryCreated = false;
	invisibleSecondaryRoads = false;
	invisibleNodes = [];
	bridgeNodes = [];
	tempNodes = [];
	nodes = [];
	primaryNodes = [];
	nodeAngles = [];
	nodeAngleDeltas = [];
	primaryIntersections = [];
	segments = {};
	segmentQueue = [];
}

//Start of road network generation
function generateRoads() {

	addNode(new THREE.Vector3(centers[1][0], getHeight(centers[1][0], centers[1][1]) , centers[1][1]), true, Math.random() * 360, 0);
	primaryIntersections.push(nodes[0]);
	growRoad(nodes[0], -90, RoadType.PRIMARY_GRID);
	growRoad(nodes[0], 0, RoadType.PRIMARY_GRID);
	growRoad(nodes[0], 90, RoadType.PRIMARY_GRID);	
	growRoad(nodes[0], 180, RoadType.PRIMARY_GRID);
	
	growPrimaryRoads();
}

//Real-time creation of road segments
function loadNextRoad() {
	
	if(segmentQueue.length > 0) {
		var road = segmentQueue.shift();
		if(road[1] == RoadType.SECONDARY_GRID && invisibleSecondaryRoads) {
			return true;
		}
		
		if(road[1] == RoadType.BRIDGE) {
			createBridge(road[0]);
		} else {
			createRoadSegment(road[0], false, road[1]);
		}
		
		return true;
	} else if(tempNodes.length > 0) {
		if(primaryCreated) {
			growSecondaryRoads();
		} else {
			growPrimaryRoads();
		}		
	} else {
		if(primaryCreated && secondaryCreated == false) {
			growSecondaryRoads();
		} else if(plots_ready) {
			return false;
		} else {
			extractCycles();
		}
	}	
}

//Add a node into nodes' array and node's angle data into angle array
function addNode(node, primary, angle, angleDelta) {
	nodes.push(node);
	
	if(primary) {
		primaryNodes.push(node);
	}
	createFlatArea(node.x, node.z, 3);
	nodeAngles.push(angle);
	nodeAngleDeltas.push(angleDelta);
}


//Attempts to create a road segment
function growRoad(startNode, angle, roadType) {
	
	var primary = (roadType == RoadType.PRIMARY_GRID || roadType == RoadType.PRIMARY_ORGANIC);
	if(primary == false) {
		secondaryNodesCreated ++;
	}
	
	var angle = angle + nodeAngles[nodes.indexOf(startNode)] % 360;
	var angleDelta = 0;
	var roadLength = segmentLength;
	switch(roadType) {
		case RoadType.PRIMARY_ORGANIC: roadLength = segmentLength; break;
		case RoadType.PRIMARY_GRID: roadLength = segmentLength; break;
		case RoadType.SECONDARY_GRID: roadLength = 70.0; break;
	}
	
	//Move the next node to segment length at given angle
	var x = startNode.x + Math.cos(toRadians(angle)) * segmentLength;
	var z = startNode.z + Math.sin(toRadians(angle)) * segmentLength;
	var x1, z1;

	//If targetNode coordinates are inside water, change the angle and find new targetNode
	if(getHeight(x, z) <= 0) {
		for(var i = 0; i < 18; i++) {
			angleDelta += 5;
			x = startNode.x + Math.cos(toRadians(angle + angleDelta)) * (segmentLength + 40);
			z = startNode.z + Math.sin(toRadians(angle + angleDelta)) * (segmentLength + 40);
			if(getHeight(x, z) > 0) {
				x1 = startNode.x + Math.cos(toRadians(angle + angleDelta)) * (segmentLength / 2);
				z1 = startNode.z + Math.sin(toRadians(angle + angleDelta)) * (segmentLength / 2);
				if(getHeight(x1, z1) > 0) {
					x = startNode.x + Math.cos(toRadians(angle + angleDelta)) * segmentLength;
					z = startNode.z + Math.sin(toRadians(angle + angleDelta)) * segmentLength;
					break;
				}
			}
			
			x = startNode.x + Math.cos(toRadians(angle - angleDelta)) * (segmentLength + 40);
			z = startNode.z + Math.sin(toRadians(angle - angleDelta)) * (segmentLength + 40);
			if(getHeight(x, z) > 0) {
				x1 = startNode.x + Math.cos(toRadians(angle - angleDelta)) * (segmentLength / 2);
				z1 = startNode.z + Math.sin(toRadians(angle - angleDelta)) * (segmentLength / 2);
				if(getHeight(x1, z1) > 0) {
					x = startNode.x + Math.cos(toRadians(angle - angleDelta)) * segmentLength;
					z = startNode.z + Math.sin(toRadians(angle - angleDelta)) * segmentLength;
					angleDelta = -angleDelta;
					break;
				}
			}
		}
	}
	
	//If targetNode coordinates are still inside water, check for bridge and larger angle
	if(getHeight(x, z) <= 0) {
		if(roadType == RoadType.SECONDARY_GRID) {
			return;
		}
		if(findBridge(startNode, angle)) {
			return;
		}
		
		//Check with real angle and larger maximum angle
		angle += nodeAngleDeltas[nodes.indexOf(startNode)];
		
		for(var i = 0; i < 20; i++) {
			
			angleDelta += 5;
			x = startNode.x + Math.cos(toRadians(angle + angleDelta)) * (segmentLength + 40);
			z = startNode.z + Math.sin(toRadians(angle + angleDelta)) * (segmentLength + 40);
			if(getHeight(x, z) >= 0) {
				x1 = startNode.x + Math.cos(toRadians(angle + angleDelta)) * (segmentLength / 2);
				z1 = startNode.z + Math.sin(toRadians(angle + angleDelta)) * (segmentLength / 2);
				if(getHeight(x1, z1) >= 0) {
					break;
				}
			}
			
			x = startNode.x + Math.cos(toRadians(angle - angleDelta)) * (segmentLength + 40);
			z = startNode.z + Math.sin(toRadians(angle - angleDelta)) * (segmentLength + 40);
			if(getHeight(x, z) >= 0) {
				x1 = startNode.x + Math.cos(toRadians(angle - angleDelta)) * (segmentLength / 2);
				z1 = startNode.z + Math.sin(toRadians(angle - angleDelta)) * (segmentLength / 2);
				if(getHeight(x1, z1) >= 0) {
					angleDelta = -angleDelta;
					break;
				}
			}
		}
		//targetNode still inside water
		if(getHeight(x, z) < 0) {
			return;
		}
	}
	
	//If new angle is big enough, it counts as intersection
	if(Math.abs(angleDelta) > 40) {	
		if(primary) {
			if(primaryIntersections.indexOf(startNode) < 0) {
				primaryIntersections.push(startNode);
			}
		} else {
			return;
		}
	}
	
	//Outside of terrain
	if(Math.abs(x) > 1270 || Math.abs(z) > 1270) {
		return;
	} else if(roadType == RoadType.SECONDARY_GRID) {
		if(Math.abs(x) > 1000 || Math.abs(z) > 1000) {
			return;
		}
	}

	var targetNode = checkSnapping(x, z, segmentLength);

	//Not snapping
	if(targetNode == null) {
		targetNode = new THREE.Vector3(x, startNode.y , z);
		if(checkSegmentIntersection(startNode, targetNode)) {
			return;
		}
		tempNodes.push(targetNode);
		addNode(targetNode, primary, angle, angleDelta);
		if(invisibleSecondaryRoads) {
			invisibleNodes.push(targetNode);
		}
		segmentQueue.push([[startNode, targetNode], roadType]);
		addSegment(startNode, targetNode);
	}
	//Snapping
	else {
		if(checkSegmentExists(startNode, targetNode)) {
			return;
		}
		if(checkSegmentIntersection(startNode, targetNode)) {
			return;
		}
		segmentQueue.push([[startNode, targetNode], roadType]);
		addSegment(startNode, targetNode);
		
		//Primary snapping to a primary creates intersection to prevent secondary roads growing from there
		if(primary) {
			if(primaryIntersections.indexOf(targetNode) < 0) {
				primaryIntersections.push(targetNode);
			}
		}
	}
}

function findBridge(startNode, angle) {
	
	angle += nodeAngleDeltas[nodes.indexOf(startNode)];
	var length = segmentLength;
	var bestAngle = angle;
	var bestLength = longestBridge + 1;
	var x, z;
	
	//Checks terrain height between -30 and 30 degrees angle at increasing length
	for(var a = -30; a <= 30; a += 5) {
		length = segmentLength + 10;
		while(length < longestBridge) {
			x = startNode.x + Math.cos(toRadians(angle + a)) * length;
			z = startNode.z + Math.sin(toRadians(angle + a)) * length;
			if(getHeight(x, z) > 0) {
				break;
			}
			length += 5;
		}
		
		if(getHeight(x, z) > 0) {
			if(length < bestLength) {
				bestLength = length;
				bestAngle = angle + a;
			}
		}
		
	}
	x = startNode.x + Math.cos(toRadians(bestAngle)) * bestLength;
	z = startNode.z + Math.sin(toRadians(bestAngle)) * bestLength;
	
	//Outside of terrain
	if(Math.abs(x) > 1270 || Math.abs(z) > 1270) {
		return false;
	}
	//Bridge too long
	if(bestLength > longestBridge) {
		return false;
	}
	
	var targetNode = targetNode = new THREE.Vector3(x, startNode.y , z);	
	
	if(checkBridgeNearby(targetNode)) {
		return false;
	}	
	if(checkSegmentExists(startNode, targetNode)) {
		return false;
	}
	
	tempNodes.push(targetNode);
	bridgeNodes.push(startNode);
	bridgeNodes.push(targetNode);
	addNode(targetNode, true, bestAngle, 0);
	
	//If bridge midpoint isn't inside water for some reason, normal road segment is created
	if(getHeight(startNode.x + Math.cos(toRadians(bestAngle)) * bestLength / 2, startNode.z + Math.sin(toRadians(bestAngle)) * bestLength / 2) >= 0) {
		addSegment(startNode, targetNode);
		segmentQueue.push([[startNode, targetNode], RoadType.PRIMARY_ORGANIC]);
		return false;
	} else {
		segmentQueue.push([[startNode, targetNode], RoadType.BRIDGE]);
	}
	
	return true;	
}


function growPrimaryRoads() {

	
	if(nodes.length > maxPrimaryNodes) {
		primaryCreated = true;
		tempNodes = [];
		return;
	}
	var currentNodes = tempNodes;
	tempNodes = [];
	
	for(p = 0; p < currentNodes.length; p++) {
		var random = Math.random();
		var roadType = RoadType.PRIMARY_GRID;
		var density = getDensity(getTerrainCoord(nodes[p].x), getTerrainCoord(nodes[p].z));
		var roadSplit = false;

		if(density < primaryGridStyleCutoffDensity) {
			roadType = RoadType.PRIMARY_ORGANIC;
		}
		if(nodes.length < maxPrimaryNodes && Math.random() < density) {
			//Left
			if(random < primaryLeftChance) {
				growRoad(currentNodes[p], 90, roadType);
				primaryIntersections.push(nodes[p]);					
			}
			//Right
			else if(random < primaryLeftChance + primaryRightChance) {
				growRoad(currentNodes[p], -90, roadType);
				primaryIntersections.push(nodes[p]);
			}
			//Both
			else if(random < primaryLeftChance + primaryRightChance + primaryBothChance) {
				growRoad(currentNodes[p], 90, roadType);
				growRoad(currentNodes[p], -90, roadType);
				primaryIntersections.push(nodes[p]);
			}
			//Split
			else if(random < primaryLeftChance + primaryRightChance + primaryBothChance + primarySplitChance && roadType != RoadType.PRIMARY_GRID) {
				growRoad(currentNodes[p], 30, roadType);
				growRoad(currentNodes[p], -30, roadType);
				primaryIntersections.push(nodes[p]);
				roadSplit = true;
			}
		}
					
		//Separate random roll for forward
		if(roadSplit == false) {
			var angle = Math.random() * 40 - 20;
			if(roadType == RoadType.PRIMARY_GRID) {
				angle = 0;
			}
			if(Math.random() < primaryStraightChance) {
				angle = 0;
			}
			growRoad(currentNodes[p], angle, roadType);
		}
		
	}
	
	if(tempNodes.length == 0) {
		primaryCreated = true;
	}
}

function growSecondaryRoads() {
	
	
	if(secondaryNodesCreated > maxSecondaryNodes) {
		if(invisibleSecondaryRoads) {
			secondaryCreated = true;
			tempNodes = [];
			return;
		} else {
			maxSecondaryNodes += maxSecondaryNodes / 2;
			invisibleSecondaryRoads = true;
		}		
	}
	
	if(tempNodes.length < 1) {	
		for(p = 0; p < nodes.length; p++) {	
			var random = Math.random();
			if(p == 0) {
				continue;
			}
			//No secondary road can start from primary intersection - removes bugs.
			if(primaryIntersections.indexOf(nodes[p]) >= 0 || bridgeNodes.indexOf(nodes[p]) >= 0	) {
				continue;
			}
			
			var angle = 90 + nodeAngleDeltas[p];
			if(Math.random() < getDensity(getTerrainCoord(nodes[p].x), getTerrainCoord(nodes[p].z))) {
				if(random < leftChance) { //Left
					growRoad(nodes[p], 90, RoadType.SECONDARY_GRID);				
				} else if(random < leftChance + rightChance) { //Right
					growRoad(nodes[p], -90, RoadType.SECONDARY_GRID);
				} else if(random < leftChance + rightChance + bothChance) { //Both
					growRoad(nodes[p], 90, RoadType.SECONDARY_GRID);
					growRoad(nodes[p], -90, RoadType.SECONDARY_GRID);
				}
			}	
		}
	} else {
		for(p = 0; p < tempNodes.length; p++) {
			var random = Math.random();		
			var roadType = RoadType.SECONDARY_GRID;
			if(secondaryNodesCreated < maxSecondaryNodes && Math.random() < getDensity(getTerrainCoord(tempNodes[p].x), getTerrainCoord(tempNodes[p].z))) {			
				var angle = 90 + nodeAngleDeltas[nodes.indexOf(tempNodes[p])];
				if(random < leftChance) {
					growRoad(tempNodes[p], angle, roadType);		
				} else if(random < leftChance + rightChance) {
					growRoad(tempNodes[p], -angle, roadType);
				} else if(random < leftChance + rightChance + bothChance) {
					growRoad(tempNodes[p], angle, roadType);
					growRoad(tempNodes[p], -angle, roadType);
				}
				//Separate random roll for forward
				if(Math.random() < forwardChance) {
					growRoad(tempNodes[p], 0, roadType);
				}
			}	
		}
		tempNodes = [];
	}
	
}


function createRoadSegment(points, createNodes, roadType) {

	//Resets points y coordinates
	if(Math.abs(points[0].y - points[1].y) > 0) {
		for(j = 0; j < points.length; j++) {
			points[j].y = points[0].y;
		}
	}
	
	

	var randomSpline =  new THREE.CatmullRomCurve3( points );
	
	var extrudeSettings = {
		steps			: Math.round(randomSpline.getLength() / 10) + 1,
		bevelEnabled	: false,
		extrudePath		: randomSpline
	};
	
	
	//Extrudes rectangle road
	var pts = [], count = 4;
	var width = secondaryRoadWidth;
	if(roadType == RoadType.PRIMARY_GRID || roadType == RoadType.PRIMARY_ORGANIC) {
		width = primaryRoadWidth;
	}
	var height = 3;
		
	pts.push( new THREE.Vector2 (height / 2, width / 2) );
	pts.push( new THREE.Vector2 (-height / 2, width / 2) );
	pts.push( new THREE.Vector2 (-height / 2, -width / 2) );
	pts.push( new THREE.Vector2 (height / 2, -width / 2) );
	
	var shape = new THREE.Shape( pts );
	var geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
	var lastNode = [points[0], 0];
	
	//Makes both sides of road equal height
	for (i = 0; i < geometry.vertices.length; i += 4) {
		geometry.vertices[i + 3].y = geometry.vertices[i].y + height;
		geometry.vertices[i + 2].y = geometry.vertices[i + 1].y + height;
	}

	//Sets road's y coordinates to terrain y coordinates.
	for (i = 0; i < geometry.vertices.length; i += 4) {
		 if(geometry.vertices[i + 1].y > geometry.vertices[i].y) {
			 var baseHeight = Math.max(getHeight(geometry.vertices[i + 3].x, geometry.vertices[i + 3].z), getHeight(geometry.vertices[i].x, geometry.vertices[i].z))  + 1;
			 geometry.vertices[i + 3].y = baseHeight;
			 geometry.vertices[i + 1].y = baseHeight + height;			 
			 geometry.vertices[i].y = baseHeight;
			 geometry.vertices[i + 2].y = baseHeight + height;
		 } else {
			 geometry.vertices[i + 1].y = getHeight(geometry.vertices[i + 1].x, geometry.vertices[i + 1].z);
			 geometry.vertices[i + 2].y = geometry.vertices[i + 1].y + height;			 
			 geometry.vertices[i + 3].y = geometry.vertices[i + 1].y + height;
			 geometry.vertices[i].y = geometry.vertices[i + 1].y;
		 }
	}
	
	geometry.computeFaceNormals();
	var material = new THREE.MeshLambertMaterial( { color: 0x313332} );
	var mesh = new THREE.Mesh( geometry, material);
	main.scene.add( mesh );
}


//Creates bridge between given points
function createBridge(points) {

	//Set points' y coords 
	if(Math.abs(points[0].y - points[1].y) > 0) {
		for(j = 0; j < points.length; j++) {
			points[j].y = points[0].y;
		}
	}

	var randomSpline =  new THREE.CatmullRomCurve3( points );
	
	var extrudeSettings = {
		steps			: Math.round(randomSpline.getLength() / 10) + 1,
		bevelEnabled	: false,
		extrudePath		: randomSpline
	};
	
	
	//Extrudes rectangle road
	var pts = [], count = 4;
	var width = primaryRoadWidth;
	var height = 6;
		
	pts.push( new THREE.Vector2 (height / 2, width / 2) );
	pts.push( new THREE.Vector2 (-height / 2, width / 2) );
	pts.push( new THREE.Vector2 (-height / 2, -width / 2) );
	pts.push( new THREE.Vector2 (height / 2, -width / 2) );
	
	var shape = new THREE.Shape( pts );
	var geometry = new THREE.ExtrudeGeometry( shape, extrudeSettings );
	var lastNode = [points[0], 0];
	
	//Makes both sides of bridge equal height
	for (i = 0; i < geometry.vertices.length; i += 4) {
		geometry.vertices[i + 3].y = geometry.vertices[i].y + height;
		geometry.vertices[i + 2].y = geometry.vertices[i + 1].y + height;
	}

	//Sets bridges's y coordinates.
	for (i = 0; i < geometry.vertices.length; i += 4) {
		
		var heightBonus = 0;
		//Creates slopes in bridge end
		if(i >= 4 && i < geometry.vertices.length - 16) {
			heightBonus = Math.min(i, 16);
		} else if(i >= geometry.vertices.length - 16) {
			heightBonus = Math.min(geometry.vertices.length - i - 4, 16);
		}
		 if(geometry.vertices[i + 1].y > geometry.vertices[i].y) {
			 geometry.vertices[i + 3].y = Math.min(getHeight(geometry.vertices[i + 3].x, geometry.vertices[i + 3].z) + heightBonus, 16);
			 geometry.vertices[i + 1].y = geometry.vertices[i + 3].y + height;			 
			 geometry.vertices[i].y = geometry.vertices[i + 3].y
			 geometry.vertices[i + 2].y = geometry.vertices[i + 3].y + height;
		 } else {
			 geometry.vertices[i + 1].y = Math.min(getHeight(geometry.vertices[i + 1].x, geometry.vertices[i + 1].z) + heightBonus, 16);
			 geometry.vertices[i + 2].y = geometry.vertices[i + 1].y + height;			 
			 geometry.vertices[i + 3].y = geometry.vertices[i + 1].y + height;
			 geometry.vertices[i].y = geometry.vertices[i + 1].y;
		 }
	}
	
	geometry.computeFaceNormals();
	var material = new THREE.MeshLambertMaterial( { color:0x313332, wireframe: false } );
	var mesh = new THREE.Mesh(geometry, material);
	main.scene.add(mesh);
}


/* Currently returns nearest node to target Position in range */
function checkSnapping(x, z, segmentLength) {
	var a;
	var snapNodes = [];
	var minDistance = -1;
	var snapNodeIndex = -1;
	for(a = 0; a < nodes.length; a++) {
		if(Math.abs(nodes[a].x - x) < segmentLength - 30) {
			if(Math.abs(nodes[a].z - z) < segmentLength - 30) {
				if(bridgeNodes.indexOf(nodes[a]) < 0) {
					snapNodes.push(nodes[a]);
				}			
			}
		}
	}
	for(var i = 0; i < snapNodes.length; i++) {
		var distance = Math.sqrt((snapNodes[i].x - x)*(snapNodes[i].x - x) + (snapNodes[i].z - z)*(snapNodes[i].z - z));
		if(minDistance == -1 || minDistance > distance) {
			minDistance = distance;
			snapNodeIndex = i;
		}
	}
	
	if(snapNodeIndex >= 0) {
		return snapNodes[snapNodeIndex];
	}
}


//Checks intersection between given segment (as nodes) and nearby segments
function checkSegmentIntersection(startNode, endNode) {
	
	var midX = (startNode.x + endNode.x) / 2;
	var midZ = (startNode.z + endNode.z) / 2;
	var startIndex = nodes.indexOf(startNode);
	var endIndex = nodes.indexOf(endNode);
	
	for(var a = 0; a < nodes.length; a++) {
		if(a == endIndex || a == startIndex) {
			continue;
		}
		if(Math.abs(nodes[a].x - midX) < segmentLength) {
			if(Math.abs(nodes[a].z - midZ) < segmentLength) {
				if(a in segments) {
					for(var k = 0; k < segments[a].length; k++) {
						if(lineIntersect(startNode.x, startNode.z, endNode.x, endNode.z, nodes[a].x, nodes[a].z, nodes[segments[a][k]].x, nodes[segments[a][k]].z)) {
							if(segments[a][k] == startIndex || segments[a][k] == endIndex) {
								continue;
							}
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}


//Add segment between given nodes to segments' dictionary
function addSegment(startNode, endNode) {
	
	var startIndex = nodes.indexOf(startNode);
	var endIndex = nodes.indexOf(endNode);
	if(startIndex == endIndex) {
		return;
	}
	
	if(startIndex < 0 || endIndex < 0) {
		createDebugNode(new THREE.Vector3(startNode.x, startNode.y, startNode.z));
		return;
	}
	
	if(startIndex in segments) {
		if(segments[startIndex].indexOf(endIndex) < 0) {
			segments[startIndex].push(endIndex);
		}		
	} else {
		segments[startIndex] = [endIndex];
	}
	
	if(endIndex in segments) {
		if(segments[endIndex].indexOf(startIndex) < 0) {
			segments[endIndex].push(startIndex);
		}	
	} else {
		segments[endIndex] = [startIndex];
	}
}


//Check if a segment between given nodes already exists
function checkSegmentExists(startNode, endNode) {
	
	var startIndex = nodes.indexOf(startNode);
	var endIndex = nodes.indexOf(endNode);
	if(startIndex in segments) {
		if(segments[startIndex].indexOf(endIndex) >= 0) {
			return true;
		}		
	}
}


//Checks whether any bridge nodes reside near given node
function checkBridgeNearby(node) {

	for(a = 0; a < bridgeNodes.length; a++) {
		if(Math.abs(bridgeNodes[a].x - node.x) < segmentLength * 1.5) {
			if(Math.abs(bridgeNodes[a].z - node.z) < segmentLength * 1.5) {
				return true;
			}
		}
	}
	return false;
}


function createDebugNode(position) {
	
	var geometry = new THREE.BoxGeometry( 10, 100, 10 );
	var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	var mesh = new THREE.Mesh( geometry, material );
	main.scene.add( mesh );
	mesh.position.x = position.x;
	mesh.position.y = position.y + 20;
	mesh.position.z = position.z;
}
