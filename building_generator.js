//Copyright (C) 2016  Kristjan Perli

//Constants
var floorHeight = 8;

//Parameters
var downtownBuildingMaxHeight = 190;
var buildingMaxHeight = 70;

//Other
var blocks = [];
var currentIndex = 0;
var blockIndex = 0;
var lotIndex = 0;
var apartmentTextures = [];
var firstFloorApartmentTextures = [];
var topFloorApartmentTextures = [];
var officeTextures = [];
var firstFloorOfficeTextures = [];
var topFloorOfficeTextures = [];
var roofTextures = [];
var loaded = false;

function resetBuildingGenerator() {
	blocks = [];
	currentIndex = 0;
	blockIndex = 0;
	lotIndex = 0;
}


function generateNextBuilding() {
	
	if(currentIndex == 0 && loaded == false) {
		loaded = true;
		 
	}
	
	//Each block contains a number of lots and block center point for translating meshes
	if(blocks[blockIndex][1].length > lotIndex) {
		generateBuilding(blocks[blockIndex][1][lotIndex], blocks[blockIndex][0]);
		lotIndex++;
	} else {
		blockIndex++;
		if(blockIndex > blocks.length - 1) {
			return false;
		}
		lotIndex = 0;
		generateBuilding(blocks[blockIndex][1][lotIndex], blocks[blockIndex][0]);
		lotIndex++;
	}
	
	currentIndex++;
	return true;	
}

//Generate a building into given plot polygon
function generateBuilding(boundingPoints, center) {
	
		var office = false;
		var points = [];
		var lowestY = -1;
		var highestY = -1;
		for(var b = 0; b < boundingPoints.length; b++) {
			points.push(new THREE.Vector2(boundingPoints[b].x, boundingPoints[b].y));
			var height = getHeight(points[b].x + center.x, points[b].y + center.y);
			if(b == 0) {
				lowestY = height;
				highestY = height;
			} else if(height < lowestY) {
				lowestY = height;
			} else if(height > highestY) {
				highestY = height;
			}
		}		

		//Extrudes rectangle road
		var height = 10;
		var density = getDensity(getTerrainCoord(points[0].x + center.x), getTerrainCoord(points[0].y + center.y));
		
		//High density area
		if(density > 0.90) {
			if(Math.random() < 0.03) {
				return;
			}
			if(Math.random() < 0.2) {
				height = Math.random() * (downtownBuildingMaxHeight - 40) + 40;
			} else {
				height = Math.random() * (downtownBuildingMaxHeight / 2 - 40) + 30;
			}		
		//Low density area
		} else {
			//Chance of not generating a building increases inversely as density decreases
			if(Math.random() > density * density + 0.2) {
				return;
			}
			if(Math.random() < density / 10) {
				height = Math.random() * (buildingMaxHeight - 30) + 30;
			} else if(density > 0.6) {
				height = Math.random() * (buildingMaxHeight - 24) + 24;
			} else {
				height = Math.random() * 10 + 24;
			}	
		}
		
		var polygonArea = calcPolygonArea(points);
		
		if(polygonArea > 600) {
			points = insetPolygon(points, Math.round(Math.random() * 4 + 1));
		}
		//Buildings that are too small aren't generated
		if(polygonArea < 250) {
			return;
		}
		
		sortVertices(points, false);
		
		//Decide the type of building	
		if(height > 100 && Math.random() < 0.5) {			
			office = true;
		} else if(height > 30 && Math.random() < 0.2) {
			office = true;
		}
		var buildingId = Math.round(Math.random() * (apartmentTextures.length - 1));
		if(office) {
			buildingId = Math.round(Math.random() * (officeTextures.length - 1));
		}
		
		createFirstFloor(points, lowestY, center, buildingId, office);
		createMiddle(points, height, lowestY, center, buildingId, office);
		createTopFloor(points, height, lowestY, center, buildingId, office);
				
		
		if(points.length == 4) {
			if(checkRectangle(points) && office == false && height < 45 && Math.random() < 0.6) {
				//Create sloped roof
				createSlopedRoof(points, lowestY, center, height, buildingId);
			} else {
				createFlatRoof(points, lowestY, center, height);
			}
		} else {
			createFlatRoof(points, lowestY, center, height);
		}
		
		
}

function checkRectangle(points) {
	if(Math.abs(Math.abs(getAngleVec2(points[0], points[1]) - getAngleVec2(points[2], points[3])) + 1 - 180) < 2) {
		if(Math.abs(Math.abs(getAngleVec2(points[1], points[2]) - getAngleVec2(points[3], points[0])) + 1 - 180) < 2) {
			return true;
		}
		return false;
	}
	return false;
}

function createFirstFloor(points, lowestY, center, buildingId, office) {
	var buildingGeometry = new THREE.Geometry();
		
	for(var i = 0; i < points.length; i++) {		
		buildingGeometry.vertices.push(
			new THREE.Vector3(points[i].x, floorHeight, points[i].y),
			new THREE.Vector3(points[i].x,  0, points[i].y)			
		);
	}

	buildingGeometry = createShape(buildingGeometry, floorHeight, false);
	
	var texture = firstFloorApartmentTextures[buildingId];
	if(office) {
		texture = firstFloorOfficeTextures[buildingId];
	}
	
	var material = new THREE.MeshLambertMaterial( { map: texture } );	
	var mesh = new THREE.Mesh(buildingGeometry, material);
	
	main.scene.add( mesh );
	mesh.position.y = lowestY;
	mesh.position.x = center.x;
	mesh.position.z = center.y;
}

function createMiddle(points, height, lowestY, center, buildingId, office) {
	var buildingGeometry = new THREE.Geometry();
		
	for(var i = 0; i < points.length; i++) {		
		buildingGeometry.vertices.push(
			new THREE.Vector3(points[i].x, height - 1.5 * floorHeight, points[i].y),
			new THREE.Vector3(points[i].x,  0, points[i].y)			
		);
	}

	buildingGeometry = createShape(buildingGeometry, height - 1.5 * floorHeight, false);
	
	var texture = apartmentTextures[buildingId];
	if(office) {
		texture = officeTextures[buildingId];
	}
	
	var material = new THREE.MeshLambertMaterial( { map: texture } );	
	var mesh = new THREE.Mesh(buildingGeometry, material);
	main.scene.add( mesh );
	mesh.position.y = lowestY + floorHeight;
	mesh.position.x = center.x;
	mesh.position.z = center.y;
}

function createTopFloor(points, height, lowestY, center, buildingId, office) {
	var buildingGeometry = new THREE.Geometry();
		
	for(var i = 0; i < points.length; i++) {		
		buildingGeometry.vertices.push(
			new THREE.Vector3(points[i].x, floorHeight * 0.5, points[i].y),
			new THREE.Vector3(points[i].x,  0, points[i].y)			
		);
	}

	buildingGeometry = createShape(buildingGeometry, floorHeight * 0.5, true);
	
	var texture = topFloorApartmentTextures[buildingId];
	if(office) {
		texture = topFloorOfficeTextures[buildingId];
	}
	
	var material = new THREE.MeshLambertMaterial( { map: texture } );	
	var mesh = new THREE.Mesh(buildingGeometry, material);
	
	main.scene.add( mesh );
	mesh.position.y = lowestY + height - floorHeight * 0.5;
	mesh.position.x = center.x;
	mesh.position.z = center.y;

}

function createShape(geometry, height, topFloor) {
	var repeatVertical = Math.round(height / floorHeight);
	var repeatHorizontal = 2;
	var minUVHeight = 0;
	if(topFloor) {
		repeatVertical = 0.48;
		minUVHeight = 0.01;
	}
	
	for(var i = 0; i < geometry.vertices.length; i += 2) {
		
		var index3 = i + 2;
		var index4 = i + 3;
		if(i == geometry.vertices.length - 2) {
			index3 = 0;
			index4 = 1;
		}
		
		repeatHorizontal = Math.round(distance(geometry.vertices[i + 1], geometry.vertices[index4]) / 8);	
		geometry.faces.push( new THREE.Face3( i, i + 1, index3) );
		geometry.faceVertexUvs[0].push([
			new THREE.Vector2( 0, minUVHeight + repeatVertical),
			new THREE.Vector2( 0, minUVHeight),
			new THREE.Vector2( repeatHorizontal, minUVHeight + repeatVertical),] );
			
		geometry.faces.push( new THREE.Face3(index4, index3, i + 1) );				
		geometry.faceVertexUvs[0].push([
			new THREE.Vector2( repeatHorizontal, minUVHeight),
			new THREE.Vector2( repeatHorizontal, minUVHeight + repeatVertical),
			new THREE.Vector2( 0, minUVHeight),] );			
	}
	
	geometry.computeFaceNormals();
	return geometry;
}

function createFlatRoof(points, lowestY, center, height) {
	
	var shapePoints = [];
	var holePoints = [];
	var roofGeometry = new THREE.Geometry();
	var roofBottomGeometry = new THREE.Geometry();
	var roofSideGeometry = new THREE.Geometry();
	var roofDepth = Math.random() + 0.7;
	var insetAmount = Math.random() + 1;
	if(Math.random() < 0.25) {
		roofDepth = -Math.random() * 1 - 0.2;
		insetAmount = Math.random() * 2 + 0.2;
	}
	
	for (var i = 0; i < points.length; i++) {
		roofGeometry.vertices.push(new THREE.Vector3(points[i].x, height, points[i].y));
		shapePoints.push(new THREE.Vector2(points[i].x, points[i].y));
		holePoints.push(new THREE.Vector2(points[i].x, points[i].y));
	}

	holePoints = insetPolygon(holePoints, insetAmount);
	var hole = new THREE.Path();
	hole.fromPoints(holePoints);

	var shape1 = new THREE.Shape(shapePoints);
	shape1.holes = [hole];
		
	for (var i = 0; i < holePoints.length; i++) {
		roofGeometry.vertices.push(new THREE.Vector3(holePoints[i].x, height, holePoints[i].y));
		roofBottomGeometry.vertices.push(new THREE.Vector3(holePoints[i].x, height - roofDepth, holePoints[i].y));
		roofSideGeometry.vertices.push(new THREE.Vector3(holePoints[i].x, height, holePoints[i].y));
		roofSideGeometry.vertices.push(new THREE.Vector3(holePoints[i].x, height - roofDepth, holePoints[i].y));
	}

	var shape1Points = shape1.extractPoints();
	var faces = THREE.ShapeUtils.triangulateShape(shape1Points.shape, shape1Points.holes);
	for (var i = 0; i < faces.length ; i++) {
		var a = faces[i][2] , b = faces[i][1] , c = faces[i][0];
		var v1 = roofGeometry.vertices[a];
		var v2 = roofGeometry.vertices[b];
		var v3 = roofGeometry.vertices[c];
		roofGeometry.faces.push( new THREE.Face3(a, b, c) );    
		roofGeometry.faceVertexUvs[0].push(
			[ new THREE.Vector2(v1.x / 5,v1.y / 5), new THREE.Vector2(v2.x / 5, v2.y / 5), new THREE.Vector2(v3.x / 5, v3.y / 5)]);
	}
	roofGeometry.computeFaceNormals();
	roofGeometry.computeVertexNormals(); 
	
	var roofTexture = roofTextures[Math.round(Math.random() * 2)];
	var roofMaterial = new THREE.MeshPhongMaterial( { map: roofTexture } );
	var roofMesh = new THREE.Mesh(roofGeometry, roofMaterial);
	
	//Lower part of roof
	var faces2 = THREE.ShapeUtils.triangulateShape(holePoints, []);
	for (var i = 0; i < faces2.length ; i++) {
		var a = faces2[i][2] , b = faces2[i][1] , c = faces2[i][0];
		var v1 = roofBottomGeometry.vertices[a];
		var v2 = roofBottomGeometry.vertices[b];
		var v3 = roofBottomGeometry.vertices[c];
		roofBottomGeometry.faces.push( new THREE.Face3(a, b, c) );    
		roofBottomGeometry.faceVertexUvs[0].push(
			[ new THREE.Vector2(v1.x / 10, v1.y / 10), new THREE.Vector2(v2.x / 10, v2.y / 10), new THREE.Vector2(v3.x / 10, v3.y / 10)]);
	}
	roofBottomGeometry.computeFaceNormals();
	roofBottomGeometry.computeVertexNormals();
	//Merge top part of roop with lower part of roof
	roofMesh.updateMatrix();
	roofBottomGeometry.merge(roofMesh.geometry, roofMesh.matrix);
	var roofBottomMesh = new THREE.Mesh(roofBottomGeometry, roofMaterial);
	
	//Side of roof (can be inside)
	for(var i = 0; i < roofSideGeometry.vertices.length; i += 2) {		
		var index3 = i + 2;
		var index4 = i + 3;
		if(i == roofSideGeometry.vertices.length - 2) {
			index3 = 0;
			index4 = 1;
		}		
		roofSideGeometry.faces.push( new THREE.Face3( i, i + 1, index3) );
		roofSideGeometry.faceVertexUvs[0].push([
			new THREE.Vector2( 0, 1),
			new THREE.Vector2( 0, 0),
			new THREE.Vector2( 1, 1),] );			
		roofSideGeometry.faces.push( new THREE.Face3(index4, index3, i + 1) );				
		roofSideGeometry.faceVertexUvs[0].push([
			new THREE.Vector2( 1, 0),
			new THREE.Vector2( 1, 1),
			new THREE.Vector2( 0, 0),] );			
	}
	
	roofBottomMesh.updateMatrix();
	roofSideGeometry.merge(roofBottomMesh.geometry, roofBottomMesh.matrix);
	var roof = new THREE.Mesh(roofSideGeometry, roofMaterial);
	main.scene.add(roof);
	roof.position.y = lowestY;
	roof.position.x = center.x;
	roof.position.z = center.y;
}

function createSlopedRoof(points, lowestY, center, height, buildingId) {
	
	var shapePoints = [];
	var roofHeight = 4;
	
	var topPoint1 = new THREE.Vector3((points[0].x + points[1].x) / 2,  height + roofHeight, (points[0].y + points[1].y) / 2);
	var topPoint2 = new THREE.Vector3((points[2].x + points[3].x) / 2,  height + roofHeight, (points[2].y + points[3].y) / 2);
	var repeatHorizontal = Math.round(distanceVec2(points[0], points[1]) / 8) * 0.001;
	var repeatVertical = roofHeight / 8 * 0.001;
	//Triangles
	var trianglesGeometry = new THREE.Geometry();
	//First side triangle
	trianglesGeometry.vertices.push(
		new THREE.Vector3(points[0].x, height, points[0].y),
		new THREE.Vector3(points[1].x,  height, points[1].y),
		topPoint1
	);
	trianglesGeometry.faces.push(new THREE.Face3(0, 1, 2));
	trianglesGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(0, 0),
				new THREE.Vector2(repeatHorizontal, 0),
				new THREE.Vector2(repeatHorizontal / 2, repeatVertical)]);
	//Second side triangle
	trianglesGeometry.vertices.push(
		new THREE.Vector3(points[2].x, height, points[2].y),
		new THREE.Vector3(points[3].x,  height, points[3].y),
		topPoint2
	);
	trianglesGeometry.faces.push(new THREE.Face3(3, 4, 5));
	trianglesGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(0, 0),
				new THREE.Vector2(repeatHorizontal, 0),
				new THREE.Vector2(repeatHorizontal / 2, repeatVertical)]);

	var texture = apartmentTextures[buildingId];
	var trianglesMaterial = new THREE.MeshPhongMaterial( { map: texture } );
	trianglesGeometry.computeFaceNormals();
	trianglesGeometry.computeVertexNormals();
	var trianglesMesh = new THREE.Mesh(trianglesGeometry, trianglesMaterial);
	main.scene.add(trianglesMesh);
	trianglesMesh.position.y = lowestY;
	trianglesMesh.position.x = center.x;
	trianglesMesh.position.z = center.y;	
	
	
	//Roof
	var roofGeometry = new THREE.Geometry();

	roofGeometry.vertices.push(
		topPoint1,
		topPoint2,
		new THREE.Vector3(points[0].x, height, points[0].y),
		new THREE.Vector3(points[1].x, height, points[1].y),
		new THREE.Vector3(points[2].x, height, points[2].y),
		new THREE.Vector3(points[3].x, height, points[3].y)
	);
	
	repeatHorizontal = Math.round(distanceVec2(points[0], points[3]) / 8) * 10;
	repeatVertical = Math.round(Math.sqrt(roofHeight * roofHeight + distanceVec2(points[0], points[1]) * distanceVec2(points[0], points[1])) / 8) * 10;
	
	//First side
	roofGeometry.faces.push(new THREE.Face3(0, 1, 2));
	roofGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(repeatHorizontal, repeatVertical),
				new THREE.Vector2(0, repeatVertical),
				new THREE.Vector2(repeatHorizontal, 0)]);
	roofGeometry.faces.push(new THREE.Face3(1, 5, 2));
	roofGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(0, repeatVertical),
				new THREE.Vector2(0, 0),
				new THREE.Vector2(repeatHorizontal, 0)]);
	//Second side
	roofGeometry.faces.push(new THREE.Face3(1, 0, 3));
	roofGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(repeatHorizontal, repeatVertical),
				new THREE.Vector2(0, repeatVertical),
				new THREE.Vector2(0, 0)]);
	roofGeometry.faces.push(new THREE.Face3(1, 3, 4));
	roofGeometry.faceVertexUvs[0].push([
				new THREE.Vector2(repeatHorizontal, repeatVertical),
				new THREE.Vector2(0, 0),
				new THREE.Vector2(repeatHorizontal, 0)]);
			
	var texture = roofTextures[Math.round(Math.random() * (roofTextures.length - 4) + 3)];
	var material = new THREE.MeshLambertMaterial( { map: texture } );
	//roofGeometry.computeFaceNormals();
	//roofGeometry.computeVertexNormals();
	mesh = new THREE.Mesh(roofGeometry, material);
	
	main.scene.add( mesh );
	mesh.position.y = lowestY;
	mesh.position.x = center.x;
	mesh.position.z = center.y;
	
	
}

function loadTextures() {
	
	//Apartment buildings
	var buildingTextureFiles = [
		"textures/apt1.png",
		"textures/apt2.png",
		"textures/apt3.png",
		"textures/apt4.png"
	];
	
	for(var i = 0; i < buildingTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(buildingTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		apartmentTextures.push(texture);
	}
	
	var firstFloorApartmentTextureFiles = [
		"textures/apt1_bottom.png",
		"textures/apt2_bottom.png",
		"textures/apt3_bottom.png",
		"textures/apt4_bottom.png"
	];
	
	for(var i = 0; i < firstFloorApartmentTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(firstFloorApartmentTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		firstFloorApartmentTextures.push(texture);
	}
	
	var topFloorApartmentTextureFiles = [
		"textures/apt1_top.png",
		"textures/apt2_top.png",
		"textures/apt3_top.png",
		"textures/apt4_top.png"
	];
	
	for(var i = 0; i < topFloorApartmentTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(topFloorApartmentTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		topFloorApartmentTextures.push(texture);
	}
	
	//Office buildings
	var officeTextureFiles = [
		"textures/off1.png",
		"textures/off2.png",
		"textures/off3.png"
	];
	
	for(var i = 0; i < officeTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(officeTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		officeTextures.push(texture);
	}
	
	var firstFloorOfficeTextureFiles = [
		"textures/off1_bottom.png",
		"textures/off2_bottom.png",
		"textures/off3_bottom.png"
	];
	
	for(var i = 0; i < firstFloorOfficeTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(firstFloorOfficeTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		firstFloorOfficeTextures.push(texture);
	}
	
	var topFloorOfficeTextureFiles = [
		"textures/off1_top.png",
		"textures/off2_top.png",
		"textures/off3_top.png"
	];
	
	for(var i = 0; i < topFloorOfficeTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(topFloorOfficeTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(1, 1);
		topFloorOfficeTextures.push(texture);
	}
	
	
	//Roofs
	var roofTextureFiles = [
		"textures/roof1.png",
		"textures/roof2.png",
		"textures/roof3.png",
		"textures/roof4.png",
		"textures/roof5.png",
		"textures/roof6.png",
		"textures/roof7.png"
	];
	
	for(var i = 0; i < roofTextureFiles.length; i++) {
		var texture = new THREE.TextureLoader().load(roofTextureFiles[i]);
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(0.1, 0.1);
		roofTextures.push(texture);
	}
}


//Sorts vertices for polygon; returns center
function sortVertices(points, substractCenter) {
		var minX = points[0].x;
		var maxX = points[0].x;
		var minY = points[0].y;
		var maxY = points[0].y;
		
		for (var i = 1; i < points.length; i++) {
			if (points[i].x < minX) minX = points[i].x;
			if (points[i].x > maxX) maxX = points[i].x;
			if (points[i].y < minY) minY = points[i].y;
			if (points[i].y > maxY) maxY = points[i].y;
		}
	
		// choose a "central" point
		var center = {
			x: minX + (maxX - minX) / 2,
			y: minY + (maxY - minY) / 2
		};

		// precalculate the angles of each point to avoid multiple calculations on sort
		for (var i = 0; i < points.length; i++) {
			points[i].angle = Math.acos((points[i].x - center.x) / lineDistance(center, points[i]));

			if (points[i].y > center.y) {
				points[i].angle = Math.PI + Math.PI - points[i].angle;
			}
		}

		// sort by angle
		points = points.sort(function(a, b) {
			return a.angle - b.angle;
		});
		
		if(substractCenter) {
			for (var i = 0; i < points.length; i++) {
				points[i].x -= center.x;
				points[i].y -= center.y;
			}
		}
		
		return center;
}

//Calculates are of polygon
function calcPolygonArea(vertices) {
    var total = 0;

    for (var i = 0, l = vertices.length; i < l; i++) {
      var addX = vertices[i].x;
      var addY = vertices[i == vertices.length - 1 ? 0 : i + 1].y;
      var subX = vertices[i == vertices.length - 1 ? 0 : i + 1].x;
      var subY = vertices[i].y;

      total += (addX * addY * 0.5);
      total -= (subX * subY * 0.5);
    }

    return Math.abs(total);
}

//Clipper library polygon insetting
var subj = new ClipperLib.Paths();
var solution = new ClipperLib.Paths();
var scale = 100;
var co = new ClipperLib.ClipperOffset(2, 0.25);

function insetPolygon(points, amount) {
	
	var path = [];
	for(var i = 0; i < points.length; i++) {
		path.push({"X":points[i].x ,"Y":points[i].y})
	}

	subj[0] = path;
	 //Have to use scale to maintain precision
	ClipperLib.JS.ScaleUpPaths(subj, scale);
	co.AddPaths(subj, ClipperLib.JoinType.jtMiter, ClipperLib.EndType.etClosedPolygon);
	co.Execute(solution, amount * -scale);
	ClipperLib.JS.ScaleDownPaths(subj, scale);
	co.Clear();

	if(solution.length == 0) {
		return [];
	}
	if(points.length == solution[0].length) {
		for(var i = 0; i < points.length; i++) {
			points[i].x = solution[0][i].X / scale;
			points[i].y = solution[0][i].Y / scale;
		}
	} else {
		points = [];
		for(var i = 0; i < solution[0].length; i++) {
			points.push(new THREE.Vector2(solution[0][i].X / scale, solution[0][i].Y / scale));
		}
	}
	return points;
}