//Copyright (C) 2016  Kristjan Perli

//Constants
var width = 512;
var height = 512;
this.max = width;
var tileLength = 5;
var heightMultiplier = 1;
var widthOffset = tileLength * width / 2;
var heightOffset = tileLength * height / 2;

//Parameters
var waterPercentMin = 0;
var waterPercentMax = 40;

//Other
var waterGenerated = false;
var canvas;
var context;
var img;
var densityHeatmap = [];
var mesh;
var generated = false;
var centers = [];
var centersTerrain = [];
var originalHeights = [];

function resetTerrain() {
	generated = false;
	centersTerrain = [];
	densityHeatmap = [];
	centers = [];
	originalHeights = [];
}

function loadTerrain() {
    canvas = document.getElementById("terrainCanvas");
    context = canvas.getContext("2d");
    context.fillStyle = "#000000";
	context.fillRect(0,0,width + 1, height + 1);
	img = context.getImageData(0, 0, width + 1, height + 1);
	waterGenerated = false;
	while(waterGenerated == false) {
		generateMidpointDisplacement();
		generateWater();
	}
	
		
	generateCenters();
	generateDensityHeatmap();
	generateMDAMesh();
};

function generateMidpointDisplacement() {
	randomizeCorners();
	for(var iteration = 9; iteration >= 1; iteration--) {
		
		var pow2 = Math.pow(2, iteration);
		var length = width / pow2;
		for(var x = 0; x < width; x += pow2) {
			for(var y = 0; y < height; y += pow2) {
				calculateMiddle(x, y, pow2, pow2);
				
				calculateVerticalSide(x, y + pow2/2, pow2/2);
				calculateVerticalSide(x + pow2, y + pow2/2, pow2/2);
				calculateHorizontalSide(x + pow2/2, y, pow2/2);
				calculateHorizontalSide(x + pow2/2, y + pow2, pow2/2);
			}
		}
	}

	context.putImageData(img, 0, 0);
}

function generateWater() {
	
	var water = 0;
	for(var x = 0; x <= width; x ++) {
		for(var y = 0; y <= height; y ++) {
			if(getPixel(x, y) - 25 > 0) {
				setPixel(x, y, getPixel(x, y) - 25);
			} else {
				setPixel(x, y, 0);
				water++;
			}
		}
	}
	
	//If generated terrain's water content isn't between set parameters, new terrain will be generated
	waterPercent = water * 100 / (width * height);
	if(waterPercent >= waterPercentMin && waterPercent <= waterPercentMax) {
		waterGenerated = true;
	}
}


function randomizeCorners() {
	setPixel(0, 0, Math.random() * 100);
	setPixel(width, 0, Math.random() * 100);
	setPixel(0, height, Math.random() * 100);
	setPixel(width, height, Math.random() * 100);
}

function calculateHorizontalSide(x, y, dist) {
	var c1 = getPixel(x - dist, y);
	var c2 = getPixel(x + dist, y);
	var avg = (c1 + c2)/2;
	
	setPixel(x, y, avg);
}

function calculateVerticalSide(x, y, dist) {

	var c1 = getPixel(x, y - dist);
	var c2 = getPixel(x, y + dist);
	var avg = (c1 + c2)/2;

	setPixel(x, y, avg);
}


function calculateMiddle(x, y, width, height) {
	var c1 = getPixel(x, y);
	var c2 = getPixel(x + width, y);
	var c3 = getPixel(x, y + height);
	var c4 = getPixel(x + width, y + height);

	var avg = (c1 + c2 + c3 + c4) / 4;	
	var rand = Math.round( (Math.random() * width / this.max - width / this.max * 0.5) * 255 * 0.8);
		
	avg += rand;	
	setPixel(x + width / 2, y + height / 2, avg);
}


function getPixel(x, y) {
	return img.data[(y * img.width + x) * 4];
}

function getHeight(x, y) {
	var height = img.data[(getTerrainCoord(y) * img.width + getTerrainCoord(x)) * 4] * heightMultiplier - 2;
	return height;
}

function setPixel(x, y, v) {
	img.data[(y * img.width + x) * 4] = v;
	img.data[(y * img.width + x) * 4 + 1] = v;
	img.data[(y * img.width + x) * 4 + 2] = v;
	originalHeights[(y * img.width + x) * 4] = v;
	originalHeights[(y * img.width + x) * 4 + 1] = v;
	originalHeights[(y * img.width + x) * 4 + 2] = v;
}


function getDistanceToCenter(x, y, center) {
	var deltaX = center[0] - x;
	var deltaY = center[1] - y;

	return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
}

//Returns population density for given terrain point
function getDensity(x, y) {
	if(Math.abs(x) > width || Math.abs(y) > height) {
		return -1;
	}
	return densityHeatmap[y * width + x];
}

//Used for smoothing road intersection areas
function createFlatArea(x, y, radius) {

	var height = img.data[(getTerrainCoord(y) * img.width + getTerrainCoord(x)) * 4] + 1;
	x = getTerrainCoord(x);
	y = getTerrainCoord(y);
	for(var i = - radius; i < radius; i++) {
		for(var j = -radius; j < radius; j++) {
			img.data[((y + j) * img.width + x + i) * 4] = height;
		}
	}
}

function resetFlatAreas() {
	for(var i = 0; i < originalHeights.length; i++) {
		img.data[i] = originalHeights[i];
	}
}


function generateMDAMesh() {

	if(mesh !== undefined) {
		scene.remove(mesh);
	}

	var material = new THREE.MeshLambertMaterial({vertexColors: THREE.FaceColors} );
	var geometry = new THREE.Geometry();
	var waterMaterial = new THREE.MeshPhongMaterial({vertexColors: THREE.FaceColors} );	
	var waterGeometry = new THREE.Geometry();

	var facesCounter = 0;
	var waterFacesCounter = 0;
	for(var x = 0; x < width; x ++) {
		for(var y = 0; y < height; y ++) {

			var p11 = getPixel(x, y);
			var p12 = getPixel(x, y + 1);
			var p21 = getPixel(x + 1, y);
			var p22 = getPixel(x + 1, y + 1);

			var baseX = x * 5 - widthOffset;
			var baseY = y * 5 - heightOffset;
	
			var blueColor = 1;
			var waterVertices = 0;
			if(p11 <= 0) { waterVertices++;}
			if(p12 <= 0) { waterVertices++;}
			if(p21 <= 0) { waterVertices++;}
			if(p22 <= 0) { waterVertices++;}

			if(waterVertices == 4) {
				//Water
				var i = waterFacesCounter;
				var j = facesCounter;
				var face1 = new THREE.Face3(i, i + 1, i + 2);
				var face2 = new THREE.Face3(i + 1, i + 3, i + 2);
				waterGeometry.vertices.push(new THREE.Vector3(baseX, p11 * heightMultiplier, baseY));
				waterGeometry.vertices.push(new THREE.Vector3(baseX, p12 * heightMultiplier, baseY + 5));
				waterGeometry.vertices.push(new THREE.Vector3(baseX + 5, p21 * heightMultiplier, baseY));
				waterGeometry.vertices.push(new THREE.Vector3(baseX + 5, p22 * heightMultiplier, baseY + 5));
				geometry.vertices.push(new THREE.Vector3(baseX, p11 * heightMultiplier - 2, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX, p12 * heightMultiplier - 2, baseY + 5));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p21 * heightMultiplier - 2, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p22 * heightMultiplier - 2, baseY + 5));
				geometry.faces.push(new THREE.Face3(j, j + 1, j + 2));
				geometry.faces.push(new THREE.Face3(j + 1, j + 3, j + 2));
				face1.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 1);
				face2.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 1);
				waterGeometry.faces.push(face1);
				waterGeometry.faces.push(face2);
				waterFacesCounter += 4;
				facesCounter += 4;
			} else if(waterVertices == 3) {
				//Coast  area
				var p11Ground = false;
				var p12Ground = false;
				var p21Ground = false;
				var p22Ground = false;
				
				geometry.vertices.push(new THREE.Vector3(baseX, p11 * heightMultiplier, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX, p12 * heightMultiplier, baseY + 5));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p21 * heightMultiplier, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p22 * heightMultiplier, baseY + 5));
				var j = facesCounter;
				var face2, face3;	
				
				if(p11 <= 0) { 
					waterGeometry.vertices.push(new THREE.Vector3(baseX, p11 * heightMultiplier, baseY));	
				} else {
					face2 = new THREE.Face3(j + 1, j + 2, j);
					p11Ground = true;}
				if(p12 <= 0) { 
					waterGeometry.vertices.push(new THREE.Vector3(baseX, p12 * heightMultiplier, baseY + 5));			
				} else {
					face2 = new THREE.Face3(j + 1, j + 3, j);
					p12Ground = true;}
				if(p21 <= 0) { 
					waterGeometry.vertices.push(new THREE.Vector3(baseX + 5, p21 * heightMultiplier, baseY));				
				} else {
					face2 = new THREE.Face3(j, j + 3, j + 2);
				}
				if(p22 <= 0) {
					waterGeometry.vertices.push(new THREE.Vector3(baseX + 5, p22 * heightMultiplier, baseY + 5));	
				} else {
					face2 = new THREE.Face3(j + 1, j + 3, j + 2);
				}
				//Water face
				var i = waterFacesCounter;
				var face1;
				if(p11Ground || p12Ground) {
					face1 = new THREE.Face3(i, i + 2, i + 1);				
				} else {
					face1 = new THREE.Face3(i, i + 1, i + 2);
				}
				face1.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 1);
				waterGeometry.faces.push(face1);
				waterFacesCounter += 3;

				face2.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 0);		
				geometry.faces.push(face2);
				geometry.faces.push(new THREE.Face3(j + 1, j + 2, j + 3));
				facesCounter += 4;					
			} else {
				var i = facesCounter;
				var face1 = new THREE.Face3(i, i + 1, i + 2);
				var face2 = new THREE.Face3(i + 1, i + 3, i + 2);
				geometry.vertices.push(new THREE.Vector3(baseX, p11 * heightMultiplier, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX, p12 * heightMultiplier, baseY + 5));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p21 * heightMultiplier, baseY));
				geometry.vertices.push(new THREE.Vector3(baseX + 5, p22 * heightMultiplier, baseY + 5));
				face1.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 0);
				face2.color.setRGB((p11 / 10) /255, (100 + p11 / 2) / 255, 0);
				geometry.faces.push(face1);
				geometry.faces.push(face2);
				facesCounter += 4;
			}		
		}
		
	}
	geometry.computeFaceNormals();
	var mesh = new THREE.Mesh( geometry, material );
	main.scene.add(mesh);
	if(waterFacesCounter > 0) {
		waterGeometry.computeFaceNormals();
		waterGeometry.computeVertexNormals();
		waterMaterial.shininess = 100;
		var waterMesh = new THREE.Mesh( waterGeometry, waterMaterial );
		main.scene.add(waterMesh);
	}
}

//Creates 3 random center points which are used to calculate density heatmap. One of the points is used as startpoint for roadmap generation.
function generateCenters() {
	var generating = true;
	var random = Math.random();
	random = Math.random();
	while (generating) {
		for(var i = 0; i < 3; i++) {
			var inWater = true;
			while (inWater) {
				var x = Math.random() * width * tileLength * 0.8 - width * tileLength * 0.4;
				var y = Math.random() * height * tileLength * 0.8 - height * tileLength * 0.4;
				if(getHeight(x, y) > 0) {
					inWater = false;
					centers.push([x, y]);
				}			
			}
		}
		generating = false;
		//Find new centers if they are too close or if they form a too shart angle
		//if(Math.abs(centers[0][0] - centers[1][0]) > 500 || Math.abs(centers[0][1] - centers[1][1]) > 500) {
			//generating = false;
			/*if(find_angle(centers[0], centers[1], centers[2]) < toRadians(50)) {
				centers = [];
			} else {
				
			}*/
		//} else {
		//	centers = [];
		//}	
	}
	for(var i = 0; i < 3; i++) {
		centersTerrain.push([getTerrainCoord(centers[i][0]), getTerrainCoord(centers[i][1])]);
	}
}


//Sets population density value for each terrain point using generated center points
function generateDensityHeatmap() {
	
	densityHeatmap = [width * height];
	for(var x = 0; x < width; x++) {
		for(var y = 0; y < height; y++) {
			for(var z = 0; z < centers.length; z++) {
				
				var distanceToCenter = getDistanceToCenter(x, y, centersTerrain[z]);
				var distanceNormalized = (1.0 - distanceToCenter/width);			
				distanceNormalized *= distanceNormalized;
				var value = 0;			
				
				if(distanceNormalized > 0.99) {
					value = 1.0;
				} else {
					value = distanceNormalized / 2;
					if(densityHeatmap[y * width + x] > 0) {
						value += densityHeatmap[y * width + x];
					}
					if(value > 0.95) {
						value = 0.95;
					}
				}
				densityHeatmap[y * width + x] = value;
			}
						
		}
	}
}